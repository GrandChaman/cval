const _ = require("lodash");
var Joi;
var default_config = {
	required: undefined // Or true or false
}

function validate_routine(method, schema, data, description, path, options, replacement, res)
{
	var error;

	if (options.required !== undefined)
		error = Joi.validate(_.get(data, path, undefined),
				Joi.reach(schema, path)[options.required ? "required" : "optional"]()
				.label(path === '' ? 'ROOT' : path)).error;
	else
		error = Joi.validate(_.get(data, path, undefined),
			Joi.reach(schema, path).label(path === '' ? 'ROOT' : path)).error;
	if (error)
		throw error;
	tmp = _.get(data, path, undefined);
	if (tmp !== undefined)
		_.set(res, path, replacement !== undefined ? replacement : tmp);
}

function validate(method, schema, data, description, path, options, replacement)
{
	var res = {};
	var error;
	var tmp;

	if (Array.isArray(method))
		method.every((val) =>
		{
			if (_.indexOf(description.tags, val) >= 0)
			{
				validate_routine(method, schema, data, description, path,
						options, replacement, res);
				return false;
			}
			return true;
		});
	else if (_.indexOf(description.tags, method) >= 0)
	{
		validate_routine(method, schema, data, description, path, options,
				replacement, res);
	}
	return res;
}

function routine(method, schema, data, options, description, path)
{
	var res = {};
	var error;

	if (description === undefined && path === undefined)
		schema = Joi.compile(schema);
	if (description === undefined)
		description = Joi.describe(schema);
	if (path === undefined)
		path = '';
	switch (description.type)
	{
		case "object":
			Object.assign(res, validate(method, schema, data, description, path, options,
				{}));
				res = routine(method, schema, data, options, description.children, path);
			break;
		case undefined:
			Object.keys(description)
				.forEach((child) => _.merge(res, routine(method, schema, data, options, description[child],
					path === '' ? child : path + '.' + child)));
			break;
		default:
			Object.assign(res, validate(method, schema, data, description, path, options));
			break;
	}
	return typeof res === "object" && _.isEmpty(res) && path !== '' ? undefined : res;
}

function gen_express_middleware(method, schema, field, options)
{
	return (req, res, next) =>
	{
		var res;

		try
		{
			res = routine(method, schema, req[field], _.assign({}, default_config, options));
			req[`original_${field}`] = req[field];
			req[field] = res;
		}
		catch (err)
		{
			next(err);
		}
		next();
	}
}

module.exports = (options, joi) =>
{
	if (joi)
		Joi = joi;
	else
		Joi = require("joi");
	return {
		create: (schema, data, options) => routine('c', schema, data, _.assign({}, default_config, options)),
		read: (schema, data, options) => routine('r', schema, data, _.assign({}, default_config, options)),
		update: (schema, data, options) => routine('u', schema, data, _.assign({}, default_config, options)),
		delete: (schema, data, options) => routine('d', schema, data, _.assign({}, default_config, options)),
		validate: (method, schema, data, options) => routine(method, schema, data, _.assign({}, default_config, options)),
		tags: {
			create: 'c',
			read: 'r',
			update: 'u',
			delete: 'd'
		},
		express:
		{
			params: (method, schema, options) => gen_express_middleware(method, schema, "params", _.assign({}, default_config, options)),
			query: (method, schema, options) => gen_express_middleware(method, schema, "query", _.assign({}, default_config, options)),
			body: (method, schema, options) => gen_express_middleware(method, schema, "body", _.assign({}, default_config, options)),
		}
	};
};