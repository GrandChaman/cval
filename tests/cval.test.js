const Joi = require("joi");
const cval = require("../lib/cval")();
const _ = require("lodash");

describe("Validate a simple object", () =>
{
	const schema = {
		a: Joi.number()
			.min(0)
			.tags(['c']),
		b: Joi.number()
			.min(3)
			.tags(['r']),
		c: Joi.number()
			.min(3)
			.tags(['d'])
	};

	const object = {
		a: 3,
		b: 4,
		c: 5
	};
	test("Validating c (correct)", () =>
	{
		var res = cval.create(schema, object);
		expect(res)
			.toEqual(
			{
				a: object.a
			});
	});
	test("Validating custom (correct)", () =>
	{
		var res = cval.validate("c", schema, object);
		expect(res)
			.toEqual(
			{
				a: object.a
			});
	});

	test("Validating custom array (correct)", () =>
	{
		var res = cval.validate(["c"], schema, object);
		expect(res)
			.toEqual(
			{
				a: object.a
			});
	});

	test("Validating c (incorrect)", () =>
	{
		expect(() => cval.create(schema, Object.assign(object,
			{
				a: -1
			})))
			.toThrow();
	});

	test("Validating custom (incorrect)", () =>
	{
		expect(() => cval.validate('c', schema, Object.assign(object,
			{
				a: -1
			})))
			.toThrow();
	});
});

describe("Validate a complex object", () =>
{
	const schema = {
		a: Joi.object(
			{
				b: Joi.number()
					.min(3)
					.tags(['d']),
				c: Joi.array()
					.items(Joi.string())
					.tags(['u'])
			})
			.tags('u')
	};

	const object = {
		a:
		{
			b: 5,
			c: ['toto', 'tutu']
		}
	};
	test("Validating c (correct)", () =>
	{
		var res = cval.update(schema, object);
		expect(res)
			.toEqual(
			{
				a:
				{
					c: object.a.c
				}
			});
	});
	test("Validating custom (correct)", () =>
	{
		var res = cval.validate("u", schema, object);
		expect(res)
			.toEqual(
			{
				a:
				{
					c: object.a.c
				}
			});
	});

	test("Validating custom array (correct)", () =>
	{
		var res = cval.validate(["u"], schema, object);
		expect(res)
			.toEqual(
			{
				a:
				{
					c: object.a.c
				}
			});
	});
	test("Validating c (incorrect)", () =>
	{
		expect(() => cval.update(schema, Object.assign(object,
			{
				a: -1
			})))
			.toThrow();
	});

	test("Validating custom (incorrect)", () =>
	{
		expect(() => cval.validate('u', schema, Object.assign(object,
			{
				a: -1
			})))
			.toThrow();
	});
});

describe("Validate some more complex object", () =>
{
	const schema = {
		a: Joi.object(
			{
				b: Joi.number()
					.min(3)
					.tags(['d']),
				c:
				{
					d: Joi.string()
						.length(10)
						.tags(['c'])
				}
			})
			.tags('u')
	};

	const object = {
		a:
		{
			b: 5,
			c:
			{
				d: "HelloWorld"
			}
		}
	};
	test("Validating c (correct)", () =>
	{
		var res = cval.create(schema, object);
		expect(res)
			.toEqual(
			{
				a:
				{
					c:
					{
						d: object.a.c.d
					}
				}
			});
	});

	test("Validating r (correct)", () =>
	{
		var res = cval.read(schema, object);
		expect(res)
			.toEqual(
			{});
	});

	test("Validating u (correct)", () =>
	{
		var res = cval.update(schema, object);
		expect(res)
			.toEqual(
			{});
	});

	test("Validating d (correct)", () =>
	{
		var res = cval.delete(schema, object);
		expect(res)
			.toEqual(
			{
				a:
				{
					b: 5
				}
			});
	});

	test("Validating empty object", () =>
	{
		var res = cval.delete(schema, {});
		expect(res)
			.toEqual(
			{
			});
	});
});

describe("Testing express middleware", () =>
{
	const schema = {
		a: Joi.number()
			.min(0)
			.tags(['c']),
		b: Joi.number()
			.min(3)
			.tags(['r']),
		c: Joi.number()
			.min(3)
			.tags(['d'])
	};

	const object = {
		a: 3,
		b: 4,
		c: 5
	};

	test.each(['query', 'params', 'body'])("Validating %s", (field, done) =>
	{
		var cb;
		var req = {};
		req[field] = object;

		cval.express[field]("c", schema)(req, {}, (err) =>
		{
			expect(err).toBe(undefined);
			expect(req[field]).toEqual(
			{
				a: object.a
			});
			done();
		});
	});
	test.each(['query', 'params', 'body'])("Validating with array of methods %s", (field, done) =>
	{
		var cb;
		var req = {};
		req[field] = object;

		cval.express[field](["c", "d"], schema)(req, {}, (err) =>
		{
			expect(err).toBe(undefined);
			expect(req[field]).toEqual(
			{
				a: object.a,
				c: object.c
			});
			done();
		});
	});
});

describe("Testing options", () =>
{
	const schema = {
		a: Joi.number()
			.min(0)
			.tags(['c']),
		b: Joi.number()
			.min(3)
			.tags(['r']),
		c: Joi.number()
			.min(3)
			.tags(['d'])
	};

	const object = {
		a: 3,
		b: 4,
		c: 5
	};

	test("Testing required options (no error)", () =>
	{
		var res = cval.create(schema, object, {required: true});
		expect(res)
			.toEqual(
			{
				a: 3
			});
	});
	test("Testing required options (error)", () =>
	{
		expect(() =>
		cval.create(schema, _.assign({}, object, {a: undefined}), {required: true}))
		.toThrow();
	});

	const schema2 = {
		a: Joi.number()
			.min(0)
			.tags(['c']).required(),
		b: Joi.number()
			.min(3)
			.tags(['c']).required(),
		c: Joi.number()
			.min(3)
			.tags(['c']).required()
	};

	test("Testing unrequired options", () =>
	{
		var res = cval.create(schema2, {a:5}, {required: false});
		expect(res)
			.toEqual(
			{
				a: 5
			});
	});

	test("Testing default options", () =>
	{
		expect(() =>
			console.log(cval.create(schema2, {a: 5}, undefined)))
			.toThrow();
	});

	const schema3 = {
		a: Joi.number()
			.min(0)
			.tags(['c']).required(),
		b: Joi.number()
			.min(3)
			.tags(['c']).required(),
		c: {
			toto: Joi.string().required().tags(['c']),
			tutu: Joi.string().required().tags(['c'])
		}
	};

	test("Testing default options", () =>
	{
		expect(cval.create(schema3, {
			a : 5,
			b : 5,
			c:{
				toto: "salut",
				tutu: "totot"
			}
		}, undefined)).toEqual({
			a : 5,
			b : 5,
			c:{
				toto: "salut",
				tutu: "totot"
			}
		})

	});
});