# CVal

This projet has goal to make request validation much easier


## Initialization

You can inizialize cval with a custom version of joi like that
```javascript
const cval = require("@grandchaman/cval")({
	required: true, /** Throw error if field with tag is missing **/
	}, /** require("custom_joi") **/);
```


## Usage

When an error occur, `cval` will throw an error. Don't forget to use `try catch`

### Prototype

```javascript
cval.create(schema, data); // => validate and trimmed object
cval.read(schema, data); // => validate and trimmed object
cval.update(schema, data); // => validate and trimmed object
cval.delete(schema, data); // => validate and trimmed object
cval.validate(method, schema, data); // => validate and trimmed object
cval.express.query(method, schema); // => express middleware for query
cval.express.params(method, schema); // => express middleware for params
cval.express.body(method, schema); // => express middleware for body
```

### Normal

You can validate content normally using the following methods :
- `create` (`"c"`)
- `read` (`"r"`)
- `update` (`"u"`)
- `delete` (`"d"`)

Then use it like that :

```javascript
const Joi = require("joi");


const schema = {
	a: Joi.string().length(10).required().tags(['c']),
	b: Joi.number().tags(['d'])
}

var res = cval.create(schema, 
	{
		a: "HelloWorld",
		b: 5
	});
/**
 * res = {
 *	a: "HelloWorld"
 * }
 */

// You can also you custom method like that
res = cval.validate('d', schema, 
	{
		a: "HelloWorld",
		b: 5
	});
/**
 * res = {
 *	b: 5
 * }
 */
```

### Express

You can use `cval` as an express middleware like so:
```javascript
const schema = {
	id: Joi.number().min(0).max(10).required()
};

router.get('/user/:id', cval.express.query('r', schema), (req, res) =>
{
	/**
	 * Your code here
	 */
});
```
